﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using CombatHandler.Generic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Desu
{
    public class CratCombatHandler : GenericCombatHandler
    {
        private static readonly List<string> AoeRootTargets = new List<string>() { "Flaming Vengeance", "Hand of the Colonel" };

        public CratCombatHandler(string pluginDir) : base(pluginDir)
        {
            settings.AddVariable("SpawnPets", true);
            settings.AddVariable("BuffPets", true);
            settings.AddVariable("UseMalaise", true);
            settings.AddVariable("UseLEInitDebuffs", true);
            settings.AddVariable("UseMalaiseOnOthers", false);
            settings.AddVariable("UseNukes", true);
            settings.AddVariable("UseAoeRoot", false);
            settings.AddVariable("BuffAuraSelection", (int)BuffAuraType.AAD);
            settings.AddVariable("UseDebuffAura", false);
            RegisterSettingsWindow("Bureaucrat Handler", "BureaucratSettingsView.xml");

            //LE Procs
            RegisterPerkProcessor(PerkHash.LEProcBureaucratWrongWindow, LEProc, CombatActionPriority.Low);
            RegisterPerkProcessor(PerkHash.LEProcBureaucratFormsInTriplicate, LEProc, CombatActionPriority.Low);

            //Spells
            RegisterSpellProcessor(RelevantNanos.PinkSlip, SingleTargetNuke, CombatActionPriority.Low);
            RegisterSpellProcessor(RelevantNanos.WorkplaceDepression, WorkplaceDepressionTargetDebuff);

            //Debuffs
            RegisterSpellProcessor(RelevantNanos.MalaiseOfZeal, CratDebuffOthersInCombat, CombatActionPriority.Low);
            RegisterSpellProcessor(RelevantNanos.MalaiseOfZeal, MalaiseTargetDebuff);
            RegisterSpellProcessor(RelevantNanos.WastefulArmMovements, LEInitTargetDebuff);
            RegisterSpellProcessor(RelevantNanos.InefficientArmMovements, LEInitTargetDebuff);
            RegisterSpellProcessor(RelevantNanos.AoeRoots, AoeRoot, CombatActionPriority.High);
            RegisterSpellProcessor(RelevantNanos.DeadMenWalking, DemotivationalAura);

            //Buffs
            RegisterSpellProcessor(RelevantNanos.AadAuras, AadAura);
            RegisterSpellProcessor(RelevantNanos.CritAuras, CritAura);
            RegisterSpellProcessor(RelevantNanos.NanoResAuras, NanoResAura);
            RegisterSpellProcessor(RelevantNanos.ImprovedCutRedTape, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.SoothingCalm, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeAttribute, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeNano, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeRanged, GenericBuff);
            RegisterSpellProcessor(RelevantNanos.CompositeRangedSpecial, GenericBuff);

            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.NanoDeltaBuffs).OrderByStackingOrder(), TeamBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.CriticalDecreaseBuff).OrderByStackingOrder(), TeamBuff);

            //Pet Buffs
            if(Spell.Find(RelevantNanos.CorporateStrategy, out Spell spell))
            {
                RegisterSpellProcessor(RelevantNanos.CorporateStrategy, CorporateStrategy);
            }
            else
            {
                RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PetShortTermDamageBuffs).OrderByStackingOrder(), PetTargetBuff);
            }
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PetDamageOverTimeResistNanos).OrderByStackingOrder(), PetTargetBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PetDefensiveNanos).OrderByStackingOrder(), PetTargetBuff);
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.PetTauntBuff).OrderByStackingOrder(), PetTargetBuff);
            RegisterSpellProcessor(RelevantNanos.DroidDamageMatrix, DroidMatrixBuff);
            RegisterSpellProcessor(RelevantNanos.DroidPressureMatrix, DroidMatrixBuff);

            //Pet Spawners
            RegisterSpellProcessor(Spell.GetSpellsForNanoline(NanoLine.SupportPets).OrderByStackingOrder(), CarloSpawner);
            RegisterSpellProcessor(RelevantNanos.Pets.Select(x => x.Key).ToArray(), RobotSpawner);

            //Pet Shells
            foreach (int shellId in RelevantNanos.Pets.Values.Select(x => x.ShellId))
            {
                RegisterItemProcessor(shellId, shellId, RobotSpawnerItem);
            }
        }

        private bool AadAura(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return BuffAura(BuffAuraType.AAD, spell, fightingTarget, ref actionTarget);
        }

        private bool CritAura(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return BuffAura(BuffAuraType.CRIT, spell, fightingTarget, ref actionTarget);
        }

        private bool NanoResAura(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return BuffAura(BuffAuraType.NANO_RES, spell, fightingTarget, ref actionTarget);
        }

        private bool BuffAura(BuffAuraType auraType, Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!IsAuraSelected(auraType))
            {
            return false;
            }

            return GenericBuff(spell, fightingTarget, ref actionTarget);
        }

        private bool AoeRoot(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!IsSettingEnabled("UseAoeRoot"))
            {
                return false;
            }

            SimpleChar target = DynelManager.Characters.Where(IsAoeRootSnareSpamTarget).Where(DoesNotHaveAoeRootRunning).FirstOrDefault();
            if (target != null)
            {
                actionTarget.Target = target;
                actionTarget.ShouldSetTarget = true;
                return true;
            }

            return false;
        }

        private bool DemotivationalAura(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if(!IsSettingEnabled("UseDebuffAura") || fightingTarget == null)
            {
                return false;
            }

            return !HasBuffNanoLine(NanoLine.EngineerDebuffAuras, DynelManager.LocalPlayer);
        }

        private bool DoesNotHaveAoeRootRunning(SimpleChar target)
        {
            return !target.Buffs.Any(IsAoeRoot);
        }

        private bool IsAoeRoot(Buff buff)
        {
            return RelevantNanos.AoeRootDebuffs.Any(id => id == buff.Identity.Instance);
        }

        private bool WorkplaceDepressionTargetDebuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!IsSettingEnabled("UseNukes"))
            {
                return false;
            }

            return NeedsDebuffRefresh(spell, fightingTarget);
        }

        private bool MalaiseTargetDebuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if(!IsSettingEnabled("UseMalaise"))
            {
                return false;
            }

            return NeedsDebuffRefresh(spell, fightingTarget);
        }

        private bool LEInitTargetDebuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!IsSettingEnabled("UseLEInitDebuffs"))
            {
                return false;
            }

            return NeedsDebuffRefresh(spell, fightingTarget);
        }

        private bool CratDebuffOthersInCombat(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget) 
        {
            return ToggledDebuffOthersInCombat("UseMalaiseOnOthers", spell, fightingTarget, ref actionTarget);
        }

        private bool NeedsDebuffRefresh(Spell spell, SimpleChar target)
        {
            if(target == null)
            {
                return false;
            }
            //Check the remaining time on debuffs. On the enemy target
            return !target.Buffs.Where(buff => buff.Name == spell.Name)
                .Where(buff => buff.RemainingTime > 1)
                .Any();
        }

        private bool SingleTargetNuke(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null || !IsSettingEnabled("UseNukes"))
            {
                return false;
            }

            return true;
        }

        protected bool DroidMatrixBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {

            if (!IsSettingEnabled("BuffPets") || !CanLookupPetsAfterZone())
            {
                return false;
            }

            Pet petToBuff = FindPetThat(pet => RobotNeedsBuff(spell, pet));
            if (petToBuff != null) {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = petToBuff.Character;
                return true;
            }

            return false;
        }

        private bool RobotNeedsBuff(Spell spell, Pet pet)
        {
            if(pet.Type != PetType.Attack)
            {
                return false;
            }

            if (FindSpellNanoLineFallbackToId(spell, pet.Character.Buffs, out Buff buff))
            {
                //Don't cast if weaker than existing
                if (spell.StackingOrder < buff.StackingOrder)
                {
                    return false;
                }

                //Don't cast if greater than 10% time remaining
                if (buff.RemainingTime / buff.TotalTime > 0.1)
                {
                    return false; ;
                }
            }

            return true;
        }

        private bool CorporateStrategy(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(NanoLine.PetShortTermDamageBuffs, PetType.Attack, spell, fightingTarget, ref actionTarget);
        }

        private bool PetTargetBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(spell.Nanoline, PetType.Attack, spell, fightingTarget, ref actionTarget);
        }

        protected bool FindSpellNanoLineFallbackToId(Spell spell, Buff[] buffs, out Buff buff)
        {
            if (buffs.Find(spell.Nanoline, out Buff buffFromNanoLine))
            {
                buff = buffFromNanoLine;
                return true;
            }
            int spellId = spell.Identity.Instance;
            if (RelevantNanos.PetNanoToBuff.ContainsKey(spellId)) { 
                int buffId = RelevantNanos.PetNanoToBuff[spellId];
                if (buffs.Find(buffId, out Buff buffFromId))
                {
                    buff = buffFromId;
                    return true;
                }
            }
            buff = null;
            return false;
        }

        protected virtual bool RobotSpawnerItem(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetSpawnerItem(RelevantNanos.Pets, item, fightingTarget, ref actionTarget);
        }

        protected bool CarloSpawner(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return NoShellPetSpawner(PetType.Support, spell, fightingTarget, ref actionTarget);
        }

        protected bool RobotSpawner(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetSpawner(RelevantNanos.Pets, spell, fightingTarget, ref actionTarget);
        }

        protected override void OnUpdate(float deltaTime)
        {
            SynchronizePetCombatStateWithOwner();

            base.OnUpdate(deltaTime);

            CancelHostileAuras(RelevantNanos.DeadMenWalking);
            if(!IsSettingEnabled("UseDebuffAura"))
            {
                CancelBuffs(RelevantNanos.DeadMenWalking);
            }
            if(!IsAuraSelected(BuffAuraType.AAD)) 
            {
                CancelBuffs(RelevantNanos.AadAuras);
            }
            if (!IsAuraSelected(BuffAuraType.CRIT))
            {
                CancelBuffs(RelevantNanos.CritAuras);
            }
            if (!IsAuraSelected(BuffAuraType.NANO_RES))
            {
                CancelBuffs(RelevantNanos.NanoResAuras);
            }
        }

        private bool IsAuraSelected(BuffAuraType auraType)
        {
            return auraType.Equals((BuffAuraType)settings["BuffAuraSelection"].AsInt32());
        }

        private static class RelevantNanos
        {
            public const int PinkSlip = 273307;
            public const int WorkplaceDepression = 273631;
            public const int MalaiseOfZeal = 275824;
            public const int WastefulArmMovements = 302150;
            public const int InefficientArmMovements = 302143;
            public const int ImprovedHeroicMeasures = 270783;
            public const int ImprovedCutRedTape = 222695;
            public const int CompositeRanged = 223348;
            public const int CompositeRangedSpecial = 223364;
            public const int CompositeNano = 223380;
            public const int CompositeAttribute = 223372;
            public const int SoothingCalm = 222853;
            public const int DroidDamageMatrix = 267916;
            public const int DroidPressureMatrix = 302247;
            public const int CorporateStrategy = 267611;
            public static readonly int[] DeadMenWalking = { 275826 };
            public static readonly int[] AoeRoots = { 224129, 224127, 224125, 224123, 224121, 224119, 82166, 82164, 82163, 82161, 82160, 82159, 82158, 82157, 82156 };
            public static readonly int[] AoeRootDebuffs = { 82137, 244634, 244633, 244630, 244631, 244632, 82138, 82139, 244629, 82140, 82141, 82142, 82143, 82144, 82145 };
            public static readonly int[] AadAuras = { 270784, 155807, 155806, 155805, 155809, 155808 };
            public static readonly int[] CritAuras = { 157503, 157499 };
            public static readonly int[] NanoResAuras = { 157504, 157500, 157501, 157502 };

            public static Dictionary<int, int> PetNanoToBuff = new Dictionary<int, int>
            {
                {DroidDamageMatrix, 285696},
                {DroidPressureMatrix, 302246},
                {CorporateStrategy, 285695}
            };

            public static Dictionary<int, PetSpellData> Pets = new Dictionary<int, PetSpellData>
            {
                { 273300, new PetSpellData(273301, PetType.Attack) },
                { 235386, new PetSpellData(239828, PetType.Attack) },
                { 46391, new PetSpellData(96213, PetType.Attack) }
            };
        }

        private enum BuffAuraType
        {
            AAD = 0,
            CRIT = 1,
            NANO_RES = 2
        }
    }
}
