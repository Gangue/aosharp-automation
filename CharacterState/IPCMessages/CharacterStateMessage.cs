﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace CombatHandler.Generic.IPCMessages
{
    [AoContract((int)IPCOpcode.CharacterState)]
    public class CharacterStateMessage : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.CharacterState;

        [AoMember(0)]
        public Identity Character { get; set; }

        [AoMember(1)]
        public int RemainingNCU { get; set; }
    }
}
