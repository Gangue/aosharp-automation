﻿using AOSharp.Character;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.IPC;
using AOSharp.Core.UI;
using Character.State;
using CombatHandler.Generic.IPCMessages;
using System;
using System.Collections.Generic;

namespace CombatHandler.Generic
{
    public class CharacterState : AOPluginEntry
    {
        public static Dictionary<Identity, int> RemainingNCU = new Dictionary<Identity, int>();

        private static IPCChannel ReportingIPCChannel;

        private static double _lastUpdateTime = 0;
        public override void Run(string pluginDir)
        {
            ReportingIPCChannel = new IPCChannel(112);

            ReportingIPCChannel.RegisterCallback((int)IPCOpcode.CharacterState, OnCharacterStateMessage);
            ReportingIPCChannel.RegisterCallback((int)IPCOpcode.Disband, OnDisband);
            Game.OnUpdate += ReportCharacterState;
            new TeamCommands().RegisterCommands();
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public static void BroadcastDisband()
        {
            ReportingIPCChannel.Broadcast(new DisbandMessage());
        }

        private static void OnCharacterStateMessage(int sender, IPCMessage msg)
        {
            try
            {
                if (Game.IsZoning)
                    return;

                CharacterStateMessage stateMessage = (CharacterStateMessage)msg;
                RemainingNCU[stateMessage.Character] = stateMessage.RemainingNCU;
            } catch(Exception e)
            {
                Chat.WriteLine(e);
            }
        }

        private static void OnDisband(int sender, IPCMessage msg)
        {
            Team.Leave();
        }

        private static void ReportCharacterState(object sender, float deltaTime)
        {
            if (Time.NormalTime - _lastUpdateTime > 1)
            {
                ReportingIPCChannel.Broadcast(new CharacterStateMessage()
                {
                    Character = DynelManager.LocalPlayer.Identity,
                    RemainingNCU = DynelManager.LocalPlayer.RemainingNCU
                });
                _lastUpdateTime = Time.NormalTime;
            }
        }
    }
}
